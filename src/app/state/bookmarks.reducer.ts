
import { createReducer, on } from '@ngrx/store';

import { Bookmark } from '../bookmark-list/bookmarks.model';
import { loadBookmarks, removeBookmark, addBookmark } from './bookmarks.actions';



export const initialState: ReadonlyArray<Bookmark> = [];

const genId = (bookmarks) => bookmarks.map(b => b.id).reduce((a, b) => Math.max(a, b) + 10, 1)


export const bookmarksReducer = createReducer(
    initialState,
    on(loadBookmarks, (state, { bookmark }) => [...bookmark]),
    on(removeBookmark, (state, { bookmarkId }) =>
        state.filter((bookmark) => bookmark.id !== bookmarkId)
    ),
    on(addBookmark, (state, { bookmark }) => {
        let b = { ...bookmark };
        b.id = genId(state);
        return [...state, b]
    })
);