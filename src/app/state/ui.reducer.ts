

import { createReducer, on } from '@ngrx/store';
import { UI } from '../bookmark-list/bookmarks.model';
import { selectSingleBookmark } from './ui.actions';

import { removeBookmark } from './bookmarks.actions';

export const initialState: UI = { selectedBookmark: null };

export const uiReducer = createReducer(
    initialState,
    on(selectSingleBookmark, (state, { Bookmark }) =>
        Object.assign({}, state, {
            selectedBookmark: Bookmark
        })
    ),
    on(removeBookmark, (state, { bookmarkId }) =>
        Object.assign({}, state, {
            selectedBookmark: null
        })
    ),
);