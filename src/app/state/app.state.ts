import { Bookmark, UI } from '../bookmark-list/bookmarks.model';

export interface AppState {
    bookmarks: ReadonlyArray<Bookmark>;
    ui: UI;
}