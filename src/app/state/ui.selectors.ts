
import { createSelector, createFeatureSelector } from "@ngrx/store";
import { AppState } from "./app.state";
import { UI } from "../bookmark-list/bookmarks.model";


export const _selectUI = (state: AppState) => state.ui;

export const selectUI = createSelector(
    _selectUI,
    (ui: UI) => ui
);

