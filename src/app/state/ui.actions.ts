
import { createAction, props } from '@ngrx/store';

export const selectSingleBookmark = createAction(
    'Select single bookmark',
    props<{ Bookmark }>()
);