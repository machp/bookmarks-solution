
import { createAction, props } from '@ngrx/store';

export const addBookmark = createAction(
    'Add Bookmark',
    props<{ bookmark }>()
);

export const removeBookmark = createAction(
    'Remove Bookmark',
    props<{ bookmarkId }>()
);


export const loadBookmarks = createAction(
    'Load Bookmarks',
    props<{ bookmark }>()
);