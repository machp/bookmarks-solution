
import { createSelector } from "@ngrx/store";
import { AppState } from "./app.state";
import { Bookmark } from "../bookmark-list/bookmarks.model";


export const selectBookmark = (state: AppState) => state.bookmarks

export const selectBookmarksFromGroup = createSelector(
    selectBookmark,
    (bookmarks: Array<Bookmark>, props) => bookmarks.filter(b => b.group == props.group)
);

export const selectBookmarkGroups = createSelector(
    selectBookmark,
    (bookmarks: Array<Bookmark>) => [...new Set(bookmarks.map(b => b.group))]
);

