
import { Bookmark, BookmarkGroup } from '../bookmark-list/bookmarks.model';

export const sampleBookmarks: Array<Bookmark> = [{
    id: 10,
    name: 'Samplelink1',
    url: 'google.com',
    group: BookmarkGroup.personal
}, {
    id: 20,
    name: 'Samplelink2',
    url: 'youtube.com',
    group: BookmarkGroup.leisure
}]