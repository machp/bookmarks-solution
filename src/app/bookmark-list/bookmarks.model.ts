// (work / leisure/ personal / ...)
export enum BookmarkGroup {
  work,
  leisure,
  personal
}

export interface Bookmark {
  id: number;
  name: string;
  url: string;
  group: BookmarkGroup;
}

export interface UI {
  selectedBookmark: Bookmark
}