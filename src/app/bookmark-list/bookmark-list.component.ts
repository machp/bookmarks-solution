import { Component, OnInit, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

import { Bookmark, BookmarkGroup } from '../bookmark-list/bookmarks.model';

@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class BookmarkListComponent implements OnInit {

  BookmarkGroup = BookmarkGroup;

  @Input() bookmarks: Array<Bookmark>;
  @Input() bookmarkGroup: number;
  @Output() select = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
