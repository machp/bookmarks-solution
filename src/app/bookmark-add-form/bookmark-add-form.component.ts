import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-bookmark-add-form',
  templateUrl: './bookmark-add-form.component.html',
  styleUrls: ['./bookmark-add-form.component.scss']
})
export class BookmarkAddFormComponent {

  @Output() add = new EventEmitter();
  constructor(private fb: FormBuilder) { }

  bookmarkForm = this.fb.group({
    name: [null, Validators.required],
    url: [null, Validators.required],
    group: [null, Validators.required],
  });

  submitted = false;

  onSubmit(): void {
    this.submitted = true;
    if (this.bookmarkForm.status == "VALID") {
      this.bookmarkForm.value.group *= 1;
      this.add.emit(this.bookmarkForm.value)
    }
  }


}
