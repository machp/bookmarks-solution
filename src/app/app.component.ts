

import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { sampleBookmarks } from './state/sample.bookmarks';

// SELECTORS
import { selectBookmarksFromGroup, selectBookmarkGroups } from './state/bookmarks.selectors';
import { selectUI } from './state/ui.selectors';


// ACTIONS
import { selectSingleBookmark } from './state/ui.actions';
import {
  addBookmark,
  removeBookmark,
  loadBookmarks,
} from './state/bookmarks.actions';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',

  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Avaloq bookmarks';

  bookmarkGroups$ = this.store.pipe(select(selectBookmarkGroups));
  ui$ = this.store.pipe(select(selectUI));
  bookmarksFromGroup$(group) {
    return this.store.pipe(select(selectBookmarksFromGroup, { group }));
  }

  onSelectSingleBookmark(Bookmark) {
    this.store.dispatch(selectSingleBookmark({ Bookmark }));
  }

  onAdd(bookmark) {
    this.store.dispatch(addBookmark({ bookmark }));
  }

  onRemove(bookmarkId) {
    this.store.dispatch(removeBookmark({ bookmarkId }));
  }

  constructor(
    private store: Store
  ) { }

  ngOnInit() {
    let bookmark = sampleBookmarks;
    this.store.dispatch(loadBookmarks({ bookmark }))
  }
}