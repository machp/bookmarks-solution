import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Bookmark, BookmarkGroup } from '../bookmark-list/bookmarks.model';
@Component({
  selector: 'app-bookmark-detail',
  templateUrl: './bookmark-detail.component.html',
  styleUrls: ['./bookmark-detail.component.scss']
})
export class BookmarkDetailComponent implements OnInit {

  BookmarkGroup = BookmarkGroup;

  @Input() bookmark: Bookmark;
  @Output() remove = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

}
